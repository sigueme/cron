import sys
import os
import time
from datetime import datetime
from importlib import import_module

from lib.config import Config

def importer(triggername):
    module = import_module('lib.triggers.{}_trigger'.format(triggername.lower()))

    return getattr(module, '{}Trigger'.format(triggername))

def main(config, run_time):
    for service in config.SERVICES:
        imported_class = importer(service)

        imported_class(config, run_time).run()

if __name__ == '__main__':
    # Load the config
    config = Config(os.path.dirname(os.path.realpath(__file__)))
    config.from_pyfile('settings.py')
    config.from_envvar('CRON_SETTINGS', silent=False)

    # Set the timezone
    os.environ['TZ'] = config.TIMEZONE
    time.tzset()

    # Logging stuff
    if not config.TESTING:
        from lib.logger import init_logging

        init_logging(config)

    # go run the code
    run_time = datetime \
        .strptime(sys.argv[1], '%Y-%m-%dT%H:%M:%S') \
        .replace(second=0)

    main(config, run_time)
