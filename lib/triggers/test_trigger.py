from . import BaseTrigger


class TestTrigger(BaseTrigger):

    def run(self):
        return 'ran on {} at {}'.format(
            self.config.TIMEZONE,
            self.run_time.strftime('%Y-%m-%d %H:%M:%S'),
        )
