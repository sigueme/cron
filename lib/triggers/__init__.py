''' The base class for cron triggers in this project '''
import json

class BaseTrigger:

    def __init__(self, config, run_time):
        self.config = config
        self.run_time = run_time

    def run():
        raise NotImplementedError('Plese override this method')

    def get_json(self, data):
        if data is None:
            return None

        else:
            return json.loads(data)

    def get_iso(self, data):
        if data is None:
            return None

        else:
            return data.replace(microsecond=0).isoformat() + 'Z'
