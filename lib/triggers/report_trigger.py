from datetime import datetime
import json
import psycopg2
import psycopg2.extras
import redis
from crontab import CronTab

from . import BaseTrigger
from ..logger import log


class ReportTrigger(BaseTrigger):

    def load_db(self):
        return psycopg2.connect(
            dbname   = self.config['POSTGRES_DB'],
            user     = self.config['POSTGRES_USER'],
            password = self.config['POSTGRES_PASSWORD'],
            host     = self.config['POSTGRES_HOST'],
            port     = self.config['POSTGRES_PORT'],
        )

    def load_redis(self):
        return redis.StrictRedis.from_url(self.config.REDIS_URL, decode_responses=True)

    def run(self):
        # Load required objects
        conn = self.load_db()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        redis = self.load_redis()

        # find scheduled reports for this minute
        cur.execute('SELECT * FROM report_scheduler WHERE run_at=%s AND active=%s;', (self.run_time, True))
        schedulers = cur.fetchall()

        # find matching cron entries
        cur.execute('SELECT * FROM report_scheduler WHERE cron IS NOT NULL AND active=%s', (True,))

        for scheduler in cur.fetchall():
            if CronTab(scheduler['cron']).test(self.run_time):
                schedulers.append(scheduler)

        for scheduler in schedulers:
            log.info('Matching scheduler {} for org {} and user {}'.format(
                scheduler['builder'],
                scheduler['org_subdomain'],
                scheduler['user_id'],
            ))

            requested_at = datetime.now()
            # insert corresponding report to database
            cur.execute("INSERT INTO report (scheduler_id, org_subdomain, status, builder, params, requested_at, completed_at, name, user_id) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)", (
                scheduler['id'],
                scheduler['org_subdomain'],
                'IN_PROGRESS',
                scheduler['builder'],
                scheduler['params'],
                requested_at,
                None,
                scheduler['name'],
                scheduler['user_id'],
            ))
            conn.commit()

            # trigger events
            redis.publish('{}:user:{}'.format(scheduler['org_subdomain'], scheduler['user_id']), json.dumps({
                'event': 'report-requested',
                'data': {
                    'report': {
                        '_type'        : '{}:report'.format(scheduler['org_subdomain']),
                        'id'           : self.last_report_id(cur),
                        'scheduler_id' : scheduler['id'],
                        'status'       : 'IN_PROGRESS',
                        'builder'      : scheduler['builder'],
                        'params'       : self.get_json(scheduler['params']),
                        'requested_at' : self.get_iso(requested_at),
                        'completed_at' : None,
                        'name'         : scheduler['name'],
                        'user_id'      : scheduler['user_id'],
                        'url'          : None,
                        'failed_reason': None,
                    },
                    'org_name': scheduler['org_subdomain'],
                },
            }))

            # update last_run_at
            cur.execute('UPDATE report_scheduler SET last_run_at=%s WHERE id=%s', (requested_at, scheduler['id']))
            conn.commit()

        # close everything
        cur.close()
        conn.close()

    def last_report_id(self, cur):
        cur.execute("SELECT last_value FROM report_id_seq")
        return cur.fetchone()['last_value']
