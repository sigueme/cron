#!/bin/bash
SCRIPT=`realpath $0`
SCRIPT_PATH=`dirname "$SCRIPT"`

cd $SCRIPT_PATH

time=`date -u +'%Y-%m-%dT%H:%M:%S'`

.env/bin/python main.py $time
