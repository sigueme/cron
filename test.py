from datetime import datetime
import json
import os
import psycopg2
import psycopg2.extras
import unittest
import redis

from main import importer
from lib.config import Config
from lib.triggers.report_trigger import ReportTrigger


class CronTestCase(unittest.TestCase):

    def load_db(self):
        return psycopg2.connect(
            dbname   = self.config['POSTGRES_DB'],
            user     = self.config['POSTGRES_USER'],
            password = self.config['POSTGRES_PASSWORD'],
            host     = self.config['POSTGRES_HOST'],
            port     = self.config['POSTGRES_PORT'],
        )

    def load_config(self):
        config = Config(os.path.dirname(os.path.realpath(__file__)))
        config.from_pyfile('settings.py')
        config.from_envvar('CRON_SETTINGS', silent=False)

        return config

    def setUp(self):
        super().setUp()

        self.config = self.load_config()
        self.conn = self.load_db()
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        self.cur.execute('DELETE FROM report_scheduler;')
        self.cur.execute('DELETE FROM report;')
        self.cur.execute('ALTER SEQUENCE report_scheduler_id_seq RESTART WITH 1;')
        self.cur.execute('ALTER SEQUENCE report_id_seq RESTART WITH 1;')

        self.conn.commit()
        self.maxDiff = None

    def tearDown(self):
        self.cur.close()
        self.conn.close()

    def test_import_modules(self):
        test_trigger = importer('Test')
        now = datetime(2018, 1, 18, 10, 54)

        self.assertEqual(test_trigger(Config('./', defaults={
            'TIMEZONE': 'ABC',
        }), now).run(), 'ran on ABC at 2018-01-18 10:54:00')

    def test_report_trigger_scheduled(self):
        ''' checks that a report requested for a specific date is actually
        triggered on time '''
        # check that no report exists
        self.cur.execute('SELECT * FROM report;')
        self.assertIsNone(self.cur.fetchone())

        # seed report scheduler
        created_at = datetime(2018, 1, 19, 12, 36)
        run_at = datetime(2018, 1, 19, 13)

        self.cur.execute('INSERT INTO report_scheduler (org_subdomain, builder, params, created_at, last_run_at, name, run_at, cron, active, user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);', (
            'testing',
            'SimpleBuilder',
            json.dumps({
                'var': 'log',
            }),
            created_at,
            None,
            'My report',
            run_at,
            None,
            True,
            'theuserid',
        ))
        self.conn.commit()

        # Subscribe to redis so we can get the events later
        r = redis.StrictRedis.from_url(self.config.REDIS_URL, decode_responses=True)
        pubsub = r.pubsub()
        pubsub.psubscribe('testing:user:*')

        # trigger trigger to test
        ReportTrigger(self.config, run_at).run()

        # check that specified report was created
        self.cur.execute('SELECT * FROM report WHERE id=%s;', (1,))
        new_report = self.cur.fetchone()
        self.assertIsNotNone(new_report)

        self.assertEqual(new_report['id'], 1)
        self.assertEqual(new_report['scheduler_id'], 1)
        self.assertEqual(new_report['org_subdomain'], 'testing')
        self.assertEqual(new_report['status'], 'IN_PROGRESS')
        self.assertEqual(new_report['builder'], 'SimpleBuilder')
        self.assertEqual(new_report['params'], '{"var": "log"}')
        self.assertTrue((new_report['requested_at']-datetime.now()).total_seconds() < 1)
        self.assertIsNone(new_report['completed_at'])
        self.assertEqual(new_report['name'], 'My report')

        # check that report-requested event is triggered
        msg = pubsub.get_message()
        self.assertEqual(msg['type'], 'psubscribe')

        msg = pubsub.get_message()
        self.assertIsNotNone(msg, 'No broker message sent')
        self.assertEqual(msg['channel'], 'testing:user:{}'.format(new_report['user_id']))

        msg_data = json.loads(msg['data'])
        self.assertTrue((datetime.strptime(msg_data['data']['report']['requested_at'], '%Y-%m-%dT%H:%M:%SZ')-datetime.now()).total_seconds() < 1)
        del msg_data['data']['report']['requested_at']

        new_report['_type'] = 'testing:report'
        new_report['params'] = json.loads(new_report['params'])
        del new_report['org_subdomain']
        del new_report['requested_at']

        self.assertDictEqual(msg_data, {
            'event': 'report-requested',
            'data': {
                'org_name': 'testing',
                'report': new_report,
            },
        })

        # check that scheduled last_run_at was updated
        self.cur.execute('select * from report_scheduler')
        scheduler = self.cur.fetchone()

        self.assertTrue((scheduler['last_run_at']-datetime.now()).total_seconds() < 1)

    def test_report_trigger_scheduled_disabled(self):
        ''' same as above but doesn't trigger due to disabled status '''
        # check that no report exists
        self.cur.execute('SELECT * FROM report;')
        self.assertIsNone(self.cur.fetchone())

        # seed report scheduler
        created_at = datetime(2018, 1, 19, 12, 36)
        run_at = datetime(2018, 1, 19, 13)

        self.cur.execute('INSERT INTO report_scheduler (org_subdomain, builder, params, created_at, last_run_at, name, run_at, cron, active, user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);', (
            'testing',
            'SimpleBuilder',
            json.dumps({
                'var': 'log',
            }),
            created_at,
            None,
            'My report',
            run_at,
            None,
            False,
            'theuserid',
        ))
        self.conn.commit()

        # Subscribe to redis so we can get the events later
        r = redis.StrictRedis.from_url(self.config.REDIS_URL, decode_responses=True)
        pubsub = r.pubsub()
        pubsub.psubscribe('testing:user:*')

        # trigger trigger to test
        ReportTrigger(self.config, run_at).run()

        # check that no report was created
        self.cur.execute('SELECT * FROM report;')
        self.assertIsNone(self.cur.fetchone())

        # check that report-requested event is not triggered
        msg = pubsub.get_message()
        self.assertEqual(msg['type'], 'psubscribe')

        msg = pubsub.get_message()
        self.assertIsNone(msg)

    def test_report_trigger_cron(self):
        ''' checks that a report with a periodical schedule is properly
        triggered on time '''
        # check that no report exists
        self.cur.execute('SELECT * FROM report;')
        self.assertIsNone(self.cur.fetchone())

        # seed report scheduler
        created_at = datetime(2018, 1, 19, 12, 36)
        run_at = datetime(2018, 2, 1, 8, 0)

        self.cur.execute('INSERT INTO report_scheduler (org_subdomain, builder, params, created_at, last_run_at, name, run_at, cron, active, user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);', (
            'testing',
            'SimpleBuilder',
            json.dumps({
                'var': 'log',
            }),
            created_at,
            None,
            'My report',
            None,
            '0 8 1 * *',
            True,
            'theuserid',
        ))
        self.conn.commit()

        # Subscribe to redis so we can get the events later
        r = redis.StrictRedis.from_url(self.config.REDIS_URL, decode_responses=True)
        pubsub = r.pubsub()
        pubsub.psubscribe('testing:user:*')

        # trigger trigger to test
        ReportTrigger(self.config, run_at).run()

        # check that specified report was created
        self.cur.execute('SELECT * FROM report WHERE id=%s;', (1,))
        new_report = self.cur.fetchone()
        self.assertIsNotNone(new_report)

        self.assertEqual(new_report['id'], 1)
        self.assertEqual(new_report['scheduler_id'], 1)
        self.assertEqual(new_report['org_subdomain'], 'testing')
        self.assertEqual(new_report['status'], 'IN_PROGRESS')
        self.assertEqual(new_report['builder'], 'SimpleBuilder')
        self.assertEqual(new_report['params'], '{"var": "log"}')
        self.assertTrue((new_report['requested_at']-datetime.now()).total_seconds() < 1)
        self.assertIsNone(new_report['completed_at'])
        self.assertEqual(new_report['name'], 'My report')

        # check that report-requested event is triggered
        msg = pubsub.get_message()
        self.assertEqual(msg['type'], 'psubscribe')

        msg = pubsub.get_message()
        self.assertIsNotNone(msg, 'No broker message sent')
        self.assertEqual(msg['channel'], 'testing:user:{}'.format(new_report['user_id']))

        msg_data = json.loads(msg['data'])
        self.assertTrue((datetime.strptime(msg_data['data']['report']['requested_at'], '%Y-%m-%dT%H:%M:%SZ')-datetime.now()).total_seconds() < 1)
        del msg_data['data']['report']['requested_at']

        new_report['_type'] = 'testing:report'
        new_report['params'] = json.loads(new_report['params'])
        del new_report['org_subdomain']
        del new_report['requested_at']

        self.assertDictEqual(msg_data, {
            'event': 'report-requested',
            'data': {
                'org_name': 'testing',
                'report': new_report,
            },
        })


if __name__ == '__main__':
    unittest.main()
