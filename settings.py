# Set the timezone to match the server's timezone
TIMEZONE = 'UTC'

# For testing environments
TESTING = False

# Each one of this is capable of triggering a set of events
SERVICES = [
    'Report',
    # Delayed trip
]

# Needed to connect to the database
POSTGRES_DB = 'fleety'
POSTGRES_USER = None
POSTGRES_PASSWORD = None
POSTGRES_HOST = 'localhost'
POSTGRES_PORT = 5432

# Needed to trigger events in redis
REDIS_URL = 'redis://localhost:6379/0'

# For error reporting
FLEETY_ERROR_CHANNEL = 'meta:server:fleety-cron'
