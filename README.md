# Fleety Cron

Triggers events based on a minute granularity.

## How to run locally

* `git clone`
* `virtualenv -p /usr/bin/python3 .env`
* `source .env/bin/activate`
* `python main.py $(date -u +'%Y-%m-%dT%H:%M:%S')`

## How to deploy

* install cron: `crontab -e`
