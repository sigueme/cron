from __future__ import with_statement
from fabric.api import run, cd, env
from fabric.context_managers import prefix
import os

WEBAPPS_ROOT = '/home/fleety/webapps'

env.hosts = ['getfleety.com']
env.user  = 'fleety'

PROJECT_PATH = os.path.join(WEBAPPS_ROOT, 'cron.getfleety.com')

def deploy():
    with cd(PROJECT_PATH):
        run('git pull')

        with prefix('source .env/bin/activate'):
            run('pip install -r requirements.txt')
